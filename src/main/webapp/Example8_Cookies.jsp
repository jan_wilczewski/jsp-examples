<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 09.10.2017
  Time: 12:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cookies</title>
</head>
<body>

<h1> Wszystkie ciasteczka: </h1>
<%
    Cookie[] cookies = request.getCookies();
    if (cookies != null){
        for (Cookie cookie: cookies){
            out.print("name: " + cookie.getName() + ", value: " + cookie.getValue());
        }
    }
%>

<h1> Ustawiamy ciasteczko: </h1>
<%
    String login = request.getParameter("login");
    if (login != null && !login.isEmpty()){
        Cookie cookie = new Cookie("cookie_login", "login");
        cookie.setMaxAge(60*60*24);
        response.addCookie(cookie);
    }
%>

<h1> Wszystkie ciasteczka raz jeszcze: </h1>
<%
    Cookie[] cookies2 = request.getCookies();
    if (cookies != null){
        for (Cookie cookie: cookies2){
            out.print("name: " + cookie.getName() + ", value: " + cookie.getValue());
        }
    }
%>

</body>
</html>
