<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 09.10.2017
  Time: 12:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UserDetails</title>
</head>
<body>

<jsp:useBean id="userInfo" class="Example7.Example7_User"></jsp:useBean>
<jsp:setProperty name="userInfo" property="*" />

You have entered below details: <br/>

Name: <jsp:getProperty name="userInfo" property="name" /><br/>
Surname: <jsp:getProperty name="userInfo" property="surname" /><br/>
Age: <jsp:getProperty name="userInfo" property="age" /><br/>
Adult: <jsp:getProperty name="userInfo" property="adult" /><br/>

</body>
</html>
