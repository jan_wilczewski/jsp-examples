<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 09.10.2017
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Example 2</title>
</head>
<body>
<h2>
    <%
        String name = request.getParameter("name");
        String timesStr = request.getParameter("times");
        int times = 1;

        if (timesStr != null){
            times = Integer.parseInt(timesStr);
        }

        if (times <= 0) {
            times = 1;
        }

        String text = "Hello, ";
        String who = "World";

        if (name != null || !name.isEmpty()){
            who = name.substring(0,1).toUpperCase() + name.substring(1);
        }

        for (int i = 0; i < times; i++) {
            out.print(text + who + "!");
            out.print("<br />");
        }
    %>
</h2>
</body>
</html>
