package Example7;

import java.io.Serializable;

public class Example7_User implements Serializable {

    private String name;
    private String surname;
    private int age;
    private boolean adult;

    public Example7_User() {
    }

    public boolean isAdult(){
        return age >= 18;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean adult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }
}
