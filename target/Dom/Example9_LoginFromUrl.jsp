<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 09.10.2017
  Time: 12:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Session</title>
</head>
<body>

<h1> Pobieramy parametr Login z URLa: </h1>
<%
    String loginFromUrl = request.getParameter("login");
    out.print("Login from URL: " + loginFromUrl);
%>

<h1> Pobieramy atrybut loginu z sesji i wyświetlamy: </h1>
<%
    Object loginFromSession = (String) session.getAttribute("login");
    out.print("Login from session: " + loginFromSession);
%>

<h1> Ustawiamy atrybut login w sesji: </h1>
<%
    session.setAttribute("login", loginFromUrl);
%>

<h1> Pobieramy atrybut login z sesji raz jeszcze: </h1>
<%
    Object loginFromSession2 = (String) session.getAttribute("login");
    out.print("Login from session: " + loginFromSession2);
%>

</body>
</html>
