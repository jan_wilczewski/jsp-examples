<%@ page import="java.time.LocalDate" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Example 1</title>
</head>
<body>

<h2>
    <%
        out.print("Hello Jan!");

        String result = "";
        for (String s: new String[]{"A", "B", "C"}) {
            result += "<h2>";
            result += s;
            result += "</h2>";
        }
    %>
    <%=result%>

    <%
        out.print(session.getId());
        String czas = LocalDate.now().toString();
    %>
</h2>

<h1> <p> Dzisiaj jest: <%= czas%></p></h1>

</body>
</html>
